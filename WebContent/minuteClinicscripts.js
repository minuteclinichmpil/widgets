var globals = 
	{
		responseJSON : "CF"
	}

function showModalForHMPIL()
{

/*    var elem = document.getElementById("widgetClinicState1");
    elem.style.display = "none";*/
    divElement = document.getElementById("modalOverlay");
    divElement.style.display = "inline";    
    divElement = document.getElementById("modalHMPIL");
    divElement.style.display = "inline";    
	
}

function hideFindAClinic()
{

    var elem = document.getElementById("widgetFindAClinic");
    elem.style.display = "none";

}

function showClinicState(addressJSON, clinicsFound)
{

    var divElement, addressLine1, addressLine2, distance;
	if(clinicsFound == "0000")
	{
		addressLine1 = addressJSON.addressLine;
		addressLine2 = addressJSON.addressCityDescriptionText + ", " + addressJSON.addressState + " " + addressJSON.addressZipCode;
		distance = addressJSON.distance;
		divElement = document.getElementById("widgetClinicState1");
		divElement.style.display = "inline";
		divElement = document.getElementById("addressLine1");
		divElement.innerHTML = addressLine1;
		divElement = document.getElementById("addressLine2");
		divElement.innerHTML = addressLine2;
		divElement = document.getElementById("distance");
		divElement.innerHTML = distance + "mi";
		divElement = document.getElementById("waittime");
		addressJSON.waittime = "30 min approx";
		divElement.innerHTML = addressJSON.waittime;
		if(addressJSON.waittime == "NULL")
		{
			divElement = document.getElementById("btnHMPILID");
			divElement.style.display = "none";    
			divElement = document.getElementById("widgetClinicState1");
			divElement.style.height = "280px";    
		}	
	}
	else
	{
		divElement = document.getElementById("widgetClinicState1");
		divElement.style.display = "inline";
		divElement = document.getElementById("addressLine1");
		divElement.innerHTML = "NO CLINICS FOUND";
		divElement = document.getElementById("addressLine2");
		divElement.innerHTML = "";
		divElement = document.getElementById("distance");
		divElement.innerHTML = "";
		divElement = document.getElementById("waittime");
		divElement.innerHTML = "NULL";
	}

}

function getClinic(position)
{

    var params, address, addressWithoutCountry, lastIndexComma, latitude, longitude, latlng, params, urlgetStoreDetails, urlGeocode, xmlhttpgetStoreDetails, xmlhttpGeocode;
	latitude  = position.coords.latitude;
    longitude = position.coords.longitude;
    latlng = latitude + ',' + longitude;
    params = "latlng=" + latlng + "&sensor=true";
	urlGeocode = 'http://maps.googleapis.com/maps/api/geocode/json?' + params;
	console.log("hello" + urlGeocode);
	urlgetStoreDetails = 'https://devservices.caremark.com:11465/minuteClinic/getStoreDetails?version=1.0&serviceName=getStoreDetails&appName=CMK_WEB&apiSecret=E228F4CF4BE33EA5A20FE5FF9D5573F8&apiKey=1008347202313004A50F01F33D27EAB1&deviceID=device12345&deviceToken=12232434&deviceType=AND_MOBILE&lineOfBusiness=PBM&channelName=MOBILE&operationName=getStoreDetails&serviceCORS=TRUE';
	if (window.XMLHttpRequest)
	{
		xmlhttpGeocode=new XMLHttpRequest();
	}
	else
	{
		xmlhttpGeocode =new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttpGeocode.onreadystatechange=function()
    {
		if (xmlhttpGeocode.readyState==4 && xmlhttpGeocode.status==200)
	    {
			data = JSON.parse(xmlhttpGeocode.responseText);
			address = (data["results"][0].formatted_address);
			lastIndexComma = address.lastIndexOf(',');
			addressWithoutCountry = address.substring(0, lastIndexComma);
			console.log(addressWithoutCountry);
			if (window.XMLHttpRequest)
			{
				xmlhttpgetStoreDetails=new XMLHttpRequest();
			}
			else
			{
				xmlhttpgetStoreDetails = new ActiveXObject("Microsoft.XMLHTTP");
			}	
			xmlhttpgetStoreDetails.onreadystatechange=function()
			{
				if (xmlhttpgetStoreDetails.readyState == 4 && xmlhttpgetStoreDetails.status == 200)
				{
					var responseJSON = JSON.parse(xmlhttpgetStoreDetails.responseText); 
					var clinicsFound = responseJSON.response.header.statusCode;
					if(clinicsFound == "0000")
					{	
					var nearestClinicJSON = responseJSON.response.details.locations[0];
					var addressJSON = 
						{
							"addressLine" : nearestClinicJSON.addressLine,
							"addressCityDescriptionText" : nearestClinicJSON.addressCityDescriptionText,
							"addressState" : nearestClinicJSON.addressState,
							"addressZipCode" : nearestClinicJSON.addressZipCode,
							"addressCountry" : nearestClinicJSON.addressCountry,
							"distance" : nearestClinicJSON.distance,
							"waittime" : nearestClinicJSON.waittime
						};
					}
					else
					{
						
					}	
					hideFindAClinic();
					showClinicState(addressJSON, clinicsFound);
				}
			}
			var jsongetStoreDetails = 
			{
					  "request": {
					    "searchCriteria": {
					      "addressLine": "245 clinton street, woonsocket, RI 02911"
					    },
					    "operation": ["clinicInfo","waittime"],
					    "services": [
					      "indicatorMinuteClinicService"
					    ]
					  }
			}
			jsongetStoreDetails.request.searchCriteria.addressLine = addressWithoutCountry;
			if(globals.responseJSON == "CF")
			{
				alert(JSON.stringify(jsongetStoreDetails));
				xmlhttpgetStoreDetails.open("POST", urlgetStoreDetails, true);
				xmlhttpgetStoreDetails.send(JSON.stringify(jsongetStoreDetails));
			}
			else if(globals.responseJSON == "CFM")
			{
				xmlhttpgetStoreDetails.open("GET", 'clinics.json', true);
				xmlhttpgetStoreDetails.send();
			}	
			else if(globals.responseJSON == "CNFM")
			{
				xmlhttpgetStoreDetails.open("GET", 'clinicsNotFound.json', true);
				xmlhttpgetStoreDetails.send();
			}	
			
	    }
	}
	xmlhttpGeocode.open("GET", urlGeocode, true);
	xmlhttpGeocode.send();    
	
}

function getClinicDetails()
{
	
	if (navigator.geolocation)
    {
        navigator.geolocation.getCurrentPosition(getClinic);
    }
/*    divElement = document.getElementById("modalOverlay");
    divElement.style.display = "inline";    
    divElement = document.getElementById("modalHMPIL");
    divElement.style.display = "inline";   */
	
}

function closeModal()
{

    divElement = document.getElementById("modalOverlay");
    divElement.style.display = "none";    
    divElement = document.getElementById("modalHMPIL");
    divElement.style.display = "none";   
	
}